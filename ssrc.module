<?php

/**
 * @file
 * Hook implementations for the Simple Secret Registration Code module.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function ssrc_help($route_name, RouteMatchInterface $route_match) {
  $output = '';
  switch ($route_name) {
    case 'ssrc.config_form':
      $output = '<p>' . t('Manage registration codes.') . '</p>';
      break;
  }

  return $output;
}

/**
 * Implements hook_form_FORM_ID_alter() for user_register_form.
 */
function ssrc_form_user_register_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  // Implement secret code field only when secret code has been configured.
  if (\Drupal::routeMatch()->getRouteName() == 'user.register'
  && \Drupal::config('simple_secret_register_code.settings')->get('secret_code_enabled')) {

    $form['ssrc_secret_code'] = [
      '#type' => 'textfield',
      '#title' => t('Registration Code'),
      '#description' => t('Use the provided code to register to this site.'),
      '#required' => TRUE,
      '#weight' => $form['actions']['submit']['#weight'] - 1,
    ];

    // Add validation handler for secret code.
    $form['#validate'][] = '_ssrc_secret_code_register_validation';
  }

  return $form;
}

/**
 * Validation handler for secret code.
 */
function _ssrc_secret_code_register_validation($form, FormStateInterface $form_state) {
  $secret_code = \Drupal::config('simple_secret_register_code.settings')->get('secret_code');
  $secret_code_input = $form_state->getValue('ssrc_secret_code');

  if (!in_array($secret_code_input, $secret_code)) {
    $form_state->setErrorByName('ssrc_secret_code', t('Registration code is not valid.'));
  }
}
