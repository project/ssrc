# Simple Seret Registration Code

This module provides a simple secret registration code feature on user 
registration form. You can add either single or multiple secret registration 
codes.

For a full description of the module, visit the [project page](https://www.drupal.org/project/ssrc).

Submit bug reports and feature suggestions, or track changes in the [issue queue](https://www.drupal.org/project/issues/ssrc).

## Requirements

This module requires no modules outside of Drupal core.

## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

1. Go to Manage Secret Registration Code page:
Configuration » Manage secret registration code (admin/config/ssrc)
2. Add secret code
3. Enable Secret Registration Code
4. Save

After these steps, all anonymous user will see the Registration
code field on user registration page.

## Maintainers

- Ryan Gambito - [Ryan Gambito](https://www.drupal.org/u/ryangambito)
