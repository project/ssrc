<?php

namespace Drupal\ssrc\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements the Simple Secret Register Code form controller.
 */
class SsrcConfig extends ConfigFormBase {

  /**
   * Build the simple form.
   *
   * A build form method constructs an array that defines how markup and
   * other form elements are included in an HTML form.
   *
   * @param array $form
   *   Default form array structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object containing current form state.
   *
   * @return array
   *   The render array defining the elements of the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ssrc.settings');
    $secret_code_saved = $config->get('secret_code');

    $form['description'] = [
      '#type' => 'item',
      '#markup' => $this->t('If multiple secret codes are added, it means either one of them will be 
      valid during user registration. Secret code added will be case sensitive'),
    ];

    $num_secret_code = $form_state->get('num_secret_code');
    if ($num_secret_code === NULL) {
      $secret_code_saved_items = ($secret_code_saved) ? count($secret_code_saved) : 1;
      $form_state->set('num_secret_code', $secret_code_saved_items);
      $num_secret_code = $secret_code_saved_items;
    }

    $form['#tree'] = TRUE;

    $form['secret_code_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Secret Registration Code'),
      '#prefix' => '<div id="secret-code-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];

    for ($i = 0; $i < $num_secret_code; $i++) {
      $form['secret_code_fieldset']['secret_code'][$i] = [
        '#type' => 'textfield',
        '#title' => $this->t('Enter Your Secret Code'),
        '#required' => TRUE,
      ];

      if (is_array($secret_code_saved) && array_key_exists($i, $secret_code_saved)) {
        $form['secret_code_fieldset']['secret_code'][$i]['#default_value'] = $secret_code_saved[$i];
      }
    }

    $form['secret_code_fieldset']['actions'] = [
      '#type' => 'actions',
    ];

    $form['secret_code_fieldset']['actions']['add_more'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add more'),
      '#submit' => ['::addOne'],
      '#ajax' => [
        'callback' => '::updateFieldCallback',
        'wrapper' => 'secret-code-fieldset-wrapper',
      ],
    ];

    if ($num_secret_code > 1) {
      $form['secret_code_fieldset']['actions']['remove'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove'),
        '#submit' => ['::removeCallback'],
        '#ajax' => [
          'callback' => '::updateFieldCallback',
          'wrapper' => 'secret-code-fieldset-wrapper',
        ],
      ];
    }

    $form['enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Secret Registration Code'),
      '#default_value' => $config->get('secret_code_enabled'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Getter method for Form ID.
   */
  public function getFormId() {
    return 'form_simple_secret_register_code_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['ssrc.settings'];
  }

  /**
   * Callback for both ajax-enabled buttons.
   */
  public function updateFieldCallback(array &$form, FormStateInterface $form_state) {
    return $form['secret_code_fieldset'];
  }

  /**
   * Add one more secret code field.
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $form_state->set('num_secret_code', $form_state->get('num_secret_code') + 1);
    $form_state->setRebuild();
  }

  /**
   * Remove one secret code field.
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    $secret_code_field = $form_state->get('num_secret_code');
    if ($secret_code_field > 1) {
      $form_state->set('num_secret_code', $secret_code_field - 1);
    }
    $form_state->setRebuild();
  }

  /**
   * Save the secret code added.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('ssrc.settings');
    $config->set('secret_code', $form_state->getValue([
      'secret_code_fieldset', 'secret_code',
    ]));
    $config->set('secret_code_enabled', $form_state->getValue('enable'));
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
